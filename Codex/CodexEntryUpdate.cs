﻿using SecretHistories.Fucine;
using SecretHistories.Fucine.DataImport;
using System.Collections.Generic;

class CodexEntryUpdate : AbstractEntity<CodexEntryUpdate>
{
    [FucineEverValue]
    public List<string> Add { get; set; }

    [FucineEverValue]
    public List<string> Remove { get; set; }

    [FucineEverValue(DefaultValue = false)]
    public bool Unlock { get; set; }

    public CodexEntryUpdate(EntityData importDataForEntity, ContentImportLog log) : base(importDataForEntity, log) { }
    protected override void OnPostImportForSpecificEntity(ContentImportLog log, Compendium populatedCompendium) { }
}

