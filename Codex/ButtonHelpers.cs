﻿using System;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Events;
using Roost.Piebald;

class ButtonHelpers
{
    public static GameObject DuplicateButton(String name, GameObject originalButton, Transform parent = null, UnityAction onClick = null, UnityAction onRightClick = null)
    {
        Transform parentTransform = parent ?? originalButton.transform.parent;
        GameObject newButton = GameObject.Instantiate(originalButton, parentTransform);
        newButton.name = name;

        ColorBlock cb = newButton.GetComponent<Button>().colors;

        GameObject.DestroyImmediate(newButton.GetComponent<ButtonSoundTrigger>());
        GameObject.DestroyImmediate(newButton.GetComponent<Button>());

        var btn = newButton.AddComponent<BetterButton>();
        btn.colors = cb;

        newButton.AddComponent<ButtonSoundTrigger>();

        btn.onClick.RemoveAllListeners();
        if (onRightClick != null)
        {
            btn.onClick.AddListener(onClick);
        }
        if (onRightClick != null)
        {
            btn.onRightClick.AddListener(onRightClick);
        }

        return newButton;
    }
}

