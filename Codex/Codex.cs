﻿using Roost;
using SecretHistories.Commands;
using SecretHistories.Commands.Encausting;
using SecretHistories.Entities;
using SecretHistories.Enums;
using SecretHistories.Fucine;
using SecretHistories.Spheres;
using SecretHistories.UI;
using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using SecretHistories.Constants;
using Roost.World.Recipes;
using SecretHistories.Abstract;
using System.IO;
using SecretHistories.Infrastructure;
using Roost.Piebald;
using Roost.World.Recipes.Entities;
using System.Collections;
using SecretHistories.Events;
using SecretHistories.Services;

class TokenComparerByLabel : Comparer<Token>
{
    public override int Compare(Token x, Token y)
    {
        var label1 = x.Payload.Label;
        var label2 = y.Payload.Label;

        return label1.CompareTo(label2);
    }
}

class Codex : MonoBehaviour
{
    public static CodexWindow codexWindow;
    public static GameObject codexButton;
    public static CodexSphere codexSphere;

    public static Coroutine scheduledRebuild = null;
    public static string selectedEntry = null;

    public static string UNLOCK_ENTRIES = "unlockCodexEntries";
    public static string UPDATE_ENTRIES = "updateCodexEntries";
    public static string REMOVE_ENTRIES = "removeCodexEntries";
    public static string CODEX = "codex";
    public static string PERSIST_CODEX = "persistCodex";
    public static string CATEGORY = "codexCategory";

    public static string CODEX_BUTTON = "codex_button";
    public static string CODEX_BUTTON_UNREAD = "codex_button_unread";

    public static CodexSphere CodexSphere
    {
        get
        {
            return codexSphere;
        }
    }

    public static void Initialise()
    {
        NoonUtility.LogWarning("Codex: Hello World.");
        AtTimeOfPower.TabletopSceneInit.Schedule(InitialiseSphere, PatchType.Postfix);
        AtTimeOfPower.TabletopSceneInit.Schedule(InitialiseCodexButton, PatchType.Postfix);
        AtTimeOfPower.TabletopSceneInit.Schedule(InitialiseCodexModal, PatchType.Postfix);

        AtTimeOfPower.NewGameSceneInit.Schedule(InitialiseSphere, PatchType.Postfix);
        AtTimeOfPower.NewGameSceneInit.Schedule(InitialiseCodexButton, PatchType.Postfix);
        AtTimeOfPower.NewGameSceneInit.Schedule(InitialiseCodexModal, PatchType.Postfix);

        Machine.ClaimProperty<Recipe, List<string>>(UNLOCK_ENTRIES);
        Machine.ClaimProperty<Recipe, List<string>>(REMOVE_ENTRIES);
        Machine.ClaimProperty<Recipe, Dictionary<string, CodexEntryUpdate>>(UPDATE_ENTRIES);

        Machine.ClaimProperty<GrandEffects, List<string>>(UNLOCK_ENTRIES);
        Machine.ClaimProperty<GrandEffects, List<string>>(REMOVE_ENTRIES);
        Machine.ClaimProperty<GrandEffects, Dictionary<string, CodexEntryUpdate>>(UPDATE_ENTRIES);

        Machine.ClaimProperty<Element, List<string>>(UNLOCK_ENTRIES);
        Machine.ClaimProperty<Element, List<string>>(REMOVE_ENTRIES);
        Machine.ClaimProperty<Element, Dictionary<string, CodexEntryUpdate>>(UPDATE_ENTRIES);
        Machine.ClaimProperty<Element, String>(CATEGORY);

        Machine.ClaimProperty<Legacy, String>(CODEX, false, "defaultcodex");
        Machine.ClaimProperty<Legacy, bool>(PERSIST_CODEX, false, false);

        // Patch game saving
        Machine.Patch(
            original: typeof(GameGateway).GetMethodInvariant(nameof(GameGateway.TryDefaultSave)),
            postfix: typeof(Codex).GetMethodInvariant(nameof(Codex.PersistCodexOnSave))
        );

        // Patch Recipe execution
        AtTimeOfPower.RecipeExecution.Schedule<Situation>(ApplyCodexEffectsFromSituation, PatchType.Prefix);

        // Patch GrandEffects resolution
        Machine.Patch(
            original: typeof(GrandEffects).GetMethodInvariant(nameof(GrandEffects.RunGrandEffects)),
            prefix: typeof(Codex).GetMethodInvariant(nameof(ApplyCodexEffectsFromGrandEffects))
        );

        // Patch Element spawn
        Machine.Patch(
            original: typeof(AbstractChronicler).GetMethodInvariant(nameof(AbstractChronicler.TokenPlacedInWorld)),
            postfix: typeof(Codex).GetMethodInvariant(nameof(ApplyCodexEffectsOnElementSpawn))
        );
    }

    // Init
    public static void InitialiseSphere()
    {
        try
        {
            FucineRoot root = FucineRoot.Get();
            Sphere potentialSphere = Watchman.Get<HornedAxe>().GetSphereByAbsolutePath(new FucinePath("*/codex"));
            if (!potentialSphere.IsValid())
            {
                codexSphere = root.DealersTable.TryCreateOrRetrieveSphere(new SphereSpec(typeof(CodexSphere), "codex")) as CodexSphere;
                Birdsong.Sing(Birdsong.Incr(), "Didn't find a valid CodexSphere. Spawned a new one=", codexSphere);

                // Try to find existing persisted codex
                // If the legacy defines it persists its codex, get the codexId and try to find the file and load it
                Legacy legacy = Watchman.Get<Stable>().Protag().ActiveLegacy;
                if (legacy == null) return;
                if (!Machine.RetrieveProperty<bool>(legacy, PERSIST_CODEX)) return;

                string codexId = Machine.RetrieveProperty<string>(legacy, CODEX);
                string persistentDataPath = Watchman.Get<MetaInfo>().PersistentDataPath;
                Directory.CreateDirectory($"{persistentDataPath}/codexes");
                string codexFilePath = $"{persistentDataPath}/codexes/{codexId}.json";

                if (!File.Exists(codexFilePath)) return;

                string json = File.ReadAllText(codexFilePath);
                SphereCreationCommand sphereContent = new SerializationHelper().DeserializeFromJsonString<SphereCreationCommand>(json);
                List<TokenCreationCommand> tokensToCreate = sphereContent.Tokens;
                tokensToCreate.ForEach(token => token.Execute(Context.Unknown(), codexSphere));
            }
            else
            {
                codexSphere = potentialSphere as CodexSphere;
                Birdsong.Sing(Birdsong.Incr(), "Found a valid existing CodexSphere =", codexSphere);
            }
            codexSphere.UpdateCategories();
        }
        catch (Exception e)
        {
            NoonUtility.LogWarning("ERROR:", e.StackTrace);
        }
    }

    public static void InitialiseCodexModal()
    {
        codexWindow = WindowFactory.CreateWindow<CodexWindow>("Window_codex");
    }

    public static void InitialiseCodexButton()
    {
        if (GameObject.Find("Button_Codex")) return;
        if (codexSphere.GetTokens().Count() == 0) return;

        GameObject buttons = GameObject.Find("Buttons");
        GameObject stackButton = GameObject.Find("Button_Stack");
        codexButton = ButtonHelpers.DuplicateButton(
            "Button_Codex",
            stackButton,
            onClick: ToggleDialog,
            onRightClick: MarkAllEntriesAsRead
        );
        Vector2 pos = codexButton.GetComponent<RectTransform>().anchoredPosition;
        pos.x = -56.7f;
        pos.y = -40;
        codexButton.GetComponent<RectTransform>().anchoredPosition = pos;
        UpdateCodexButton();
    }

    public static void UpdateCodexButton()
    {
        if (!codexButton) InitialiseCodexButton();
        string spriteToUse = codexSphere.HasUnreadEntries ? CODEX_BUTTON_UNREAD : CODEX_BUTTON;
        codexButton.GetComponent<Image>().sprite = ResourcesManager.GetSpriteForUI(spriteToUse);
    }

    public static void PersistCodexOnSave()
    {
        Legacy legacy = Watchman.Get<Stable>().Protag().ActiveLegacy;
        if (legacy == null) return;
        if (!Machine.RetrieveProperty<bool>(legacy, PERSIST_CODEX)) return;

        SphereCreationCommand encaustment = new Encaustery<SphereCreationCommand>().Encaust(codexSphere);
        SerializationHelper serializationHelper = new SerializationHelper();
        string sphereContentAsJson = serializationHelper.SerializeToJsonString(encaustment);

        string codexId = Machine.RetrieveProperty<string>(legacy, CODEX);
        string persistentDataPath = Watchman.Get<MetaInfo>().PersistentDataPath;
        Directory.CreateDirectory($"{persistentDataPath}/codexes");
        string codexFilePath = $"{persistentDataPath}/codexes/{codexId}.json";

        File.WriteAllText(codexFilePath, sphereContentAsJson);
    }

    // Element
    public static void ApplyCodexEffectsOnElementSpawn(Token token)
    {
        Element element = Watchman.Get<Compendium>().GetEntityById<Element>(token.Payload.EntityId);
        var entriesToUnlock = Machine.RetrieveProperty<List<string>>(element, UNLOCK_ENTRIES);
        var entriesToRemove = Machine.RetrieveProperty<List<string>>(element, REMOVE_ENTRIES);
        var editionInformations = Machine.RetrieveProperty<Dictionary<string, CodexEntryUpdate>>(element, UPDATE_ENTRIES);
        if (entriesToUnlock != null) UnlockEntries(entriesToUnlock);
        if (entriesToRemove != null) RemoveEntries(entriesToRemove);
        if (editionInformations != null) UpdateEntries(editionInformations);
    }

    // Recipe
    public static void ApplyCodexEffectsFromSituation(Situation situation)
    {
        var entriesToUnlock = situation.CurrentRecipe.RetrieveProperty<List<string>>(UNLOCK_ENTRIES);
        var entriesToRemove = situation.CurrentRecipe.RetrieveProperty<List<string>>(REMOVE_ENTRIES);
        var editionInformations = situation.CurrentRecipe.RetrieveProperty<Dictionary<string, CodexEntryUpdate>>(UPDATE_ENTRIES);
        if (entriesToUnlock != null) UnlockEntries(entriesToUnlock);
        if (entriesToRemove != null) RemoveEntries(entriesToRemove);
        if (editionInformations != null) UpdateEntries(editionInformations);
    }

    // GrandEffects
    public static void ApplyCodexEffectsFromGrandEffects(GrandEffects __instance)
    {
        var entriesToUnlock = __instance.RetrieveProperty<List<string>>(UNLOCK_ENTRIES);
        var entriesToRemove = __instance.RetrieveProperty<List<string>>(REMOVE_ENTRIES);
        var editionInformations = __instance.RetrieveProperty<Dictionary<string, CodexEntryUpdate>>(UPDATE_ENTRIES);
        if (entriesToUnlock != null) UnlockEntries(entriesToUnlock);
        if (entriesToRemove != null) RemoveEntries(entriesToRemove);
        if (editionInformations != null) UpdateEntries(editionInformations);
    }

    // Underlying operations
    public static List<Token> UnlockEntries(List<string> entriesToUnlock)
    {
        Compendium compendium = Watchman.Get<Compendium>();
        List<Token> createdEntries = new();
        foreach (string entryId in entriesToUnlock)
        {
            Element element = compendium.GetEntityById<Element>(entryId);
            if (element == null) continue;
            if (codexSphere.GetElementToken(entryId) != null) continue;

            string category = element.RetrieveProperty<string>(CATEGORY);
            if(category != null && codexSphere.GetElementToken(category) == null)
            {
                Birdsong.Sing(Birdsong.Incr(), $"{entryId} has a parent category, {category}, that isn't already unlocked. Unlocking it.");
                var categoryToken = UnlockEntries(new() { category })[0];
                categoryToken.Payload.SetMutation("codex.category.open", 1, false);
            }

            Token newToken = codexSphere.ProvisionElementToken(entryId, 1);
            newToken.Payload.SetMutation("codex.unread", 1, false);
            Birdsong.Sing(Birdsong.Incr(), $"Unlocked entry {entryId}.");
            createdEntries.Add(newToken);
        }
        return createdEntries;
    }

    public static void RemoveEntries(List<string> entriesToRemove)
    {
        Compendium compendium = Watchman.Get<Compendium>();
        foreach (string entryId in entriesToRemove)
        {
            if (!compendium.EntityExists<Element>(entryId)) continue;
            if (codexSphere.GetElementToken(entryId) == null) continue;
            codexSphere.ReduceElement(entryId, 1);
        }
    }

    public static void UpdateEntries(Dictionary<string, CodexEntryUpdate> editionInformations)
    {
        Compendium compendium = Watchman.Get<Compendium>();
        foreach (var entryInfo in editionInformations)
        {
            var entryId = entryInfo.Key;
            var operations = entryInfo.Value;

            if (!compendium.EntityExists<Element>(entryId))
            {
                Birdsong.Sing(Birdsong.Incr(), $"Tried doing operations on element entry '{entryId}' but this element is unknown by the Compendium. Moving on.");
                continue;
            }
            if (!compendium.GetEntityById<Element>(entryId).IsValid())
            {
                Birdsong.Sing(Birdsong.Incr(), $"Tried doing operations on element entry '{entryId}' but this element is not present in the codex sphere. Moving on.");
                continue;
            }

            Token token = codexSphere.GetElementToken(entryId);
            if (!token)
            {
                if (!operations.Unlock) continue;
                List<string> entries = new List<string>() { entryId };
                UnlockEntries(entries);
            }
            foreach (var aspectId in operations.Add)
            {
                Birdsong.Sing(Birdsong.Incr(), $"Adding the aspect '{aspectId}' on element entry '{entryId}'.");
                if (!compendium.EntityExists<Element>(aspectId))
                {
                    Birdsong.Sing(Birdsong.Incr(), $"Tried adding the aspect '{aspectId}' on element entry '{entryId}' but this aspect is unknown by the Compendium. Moving on.");
                    continue;
                }
                token.Payload.SetMutation(aspectId, 1, false);
                token.Payload.SetMutation("codex.unread", 1, false);
            }
            foreach (var aspectId in operations.Remove)
            {
                Birdsong.Sing(Birdsong.Incr(), $"Removing the aspect '{aspectId}' on element entry '{entryId}'.");
                if (!compendium.EntityExists<Element>(aspectId))
                {
                    Birdsong.Sing(Birdsong.Incr(), $"Tried removing the aspect '{aspectId}' on element entry '{entryId}' but this aspect is unknown. Moving on.");
                    continue;
                }
                token.Payload.SetMutation(aspectId, 0, false);
                token.Payload.SetMutation("codex.unread", 1, false);
            }
        }
    }

    // Dialog state handling
    public static bool DialogIsOpen()
    {
        return codexWindow != null && codexWindow.IsOpen;
    }

    public static void ToggleDialog()
    {
        if (!DialogIsOpen())
        {
            OpenDialog();
        }
        else
        {
            CloseDialog();
        }
    }

    public static void OpenDialog()
    {
        Watchman.Get<LocalNexus>().ForcePauseGame(false);
        codexWindow.OpenAt(Vector2.zero);
    }

    public static void CloseDialog()
    {
        codexWindow.Close();
        Watchman.Get<LocalNexus>().UnForcePauseGame(false);
    }

    public static void MarkAllEntriesAsRead()
    {
        codexSphere.MarkAllEntriesAsRead();
    }

    public static void MarkEntryAsRead(Token entry)
    {
        codexSphere.MarkEntryAsRead(entry);
    }

    public static void ScheduleUIUpdate()
    {
        Birdsong.Sing("Scheduling Codex UI update...");
        if (scheduledRebuild == null && codexSphere != null)
        {
            Birdsong.Sing("Scheduled");
            scheduledRebuild = codexSphere.StartCoroutine(UpdateUI());
        }
    }

    // UI building
    public static IEnumerator UpdateUI()
    {
        yield return null;
        if (codexWindow != null) codexWindow.UpdateUI();
        UpdateCodexButton();
        scheduledRebuild = null;
        Birdsong.Sing("Updated Codex UI");
    }

    // Interactions
    public void Update()
    {
        if (Keyboard.current[Key.H].wasPressedThisFrame && !(Watchman.Get<LocalNexus>() as UIController).IsEditingText())
        {
            //ToggleDialog();
        }
        if (Keyboard.current[Key.Escape].wasPressedThisFrame && DialogIsOpen()) CloseDialog();
    }
}
