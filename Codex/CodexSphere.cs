﻿using Roost;
using SecretHistories.Commands;
using SecretHistories.Entities;
using SecretHistories.Enums;
using SecretHistories.Events;
using SecretHistories.Spheres;
using SecretHistories.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Events;
using static UnityEngine.UI.Button;

[IsEmulousEncaustable(typeof(Sphere))]
class CodexSphere : Sphere
{
    public override SphereCategory SphereCategory => SphereCategory.Dormant;

    public Dictionary<Token, List<Token>> Categories = new();
    public Dictionary<Token, int> UnreadCategories = new();
    public List<Token> EntriesWithoutCategory = new();
    public bool HasUnreadEntries = false;

    public void MarkEntryAsUnread(Token token)
    {
        MarkEntriesAsUnread(new List<Token> { token });
    }

    public void MarkAllEntriesAsUnread()
    {
        MarkEntriesAsUnread(GetTokens());
    }

    private void MarkEntriesAsUnread(IEnumerable<Token> entries)
    {
        foreach (Token token in entries)
        {
            token.Payload.SetMutation("codex.unread", 1, false);
        }
    }

    public void MarkEntryAsRead(Token token)
    {
        MarkEntriesAsRead(new List<Token>{ token });
    }

    public void MarkAllEntriesAsRead()
    {
        MarkEntriesAsRead(GetTokens());
    }

    private void MarkEntriesAsRead(IEnumerable<Token> entries)
    {
        foreach (Token token in entries)
        {
            if(token.GetCurrentMutations().ContainsKey("codex.unread")) token.Payload.SetMutation("codex.unread", 0, false);
        }
    }

    public override void NotifyTokensChangedForSphere(SphereContentsChangedEventArgs args)
    {
        UpdateCategories();
        base.NotifyTokensChangedForSphere(args);
        Codex.ScheduleUIUpdate();
    }

    public void UpdateCategories()
    {
        Categories.Clear();
        UnreadCategories.Clear();
        EntriesWithoutCategory.Clear();
        HasUnreadEntries = false;

        Compendium compendium = Watchman.Get<Compendium>();
        // 1. Get all the categories, because we need to forbid tokens representing categories to be put into other categories
        List<string> categoryIds = Tokens.ConvertAll(t =>
        {
            Element element = compendium.GetEntityById<Element>(t.PayloadEntityId);
            if (element == null) return null;
            return element.RetrieveProperty<string>(Codex.CATEGORY);
        }).FindAll(id => id != null);

        Birdsong.Sing(Birdsong.Incr(), $"Category ids: [{String.Join(";", categoryIds)}]");

        List<Token> categoriesTokens = Tokens.FindAll(t => categoryIds.Contains(t.PayloadEntityId));

        foreach(Token category in categoriesTokens)
        {
            Categories.Add(category, new List<Token>());
            if (category.GetCurrentMutations().ContainsKey("codex.unread"))
            {
                UnreadCategories.Add(category, 1);
                HasUnreadEntries = true;
            }
            else UnreadCategories.Add(category, 0);
        }

        foreach (Token token in Tokens)
        {
            Element element = compendium.GetEntityById<Element>(token.PayloadEntityId);
            if (element == null) continue;

            Birdsong.Sing(Birdsong.Incr(), $"Processing {token.PayloadEntityId}...");

            // This token is actually a category. Skip it.
            if (categoryIds.Contains(element.Id))
            {
                Birdsong.Sing(Birdsong.Incr(), $"It is a category. Skipping.");
                continue;
            }

            Birdsong.Sing(Birdsong.Incr(), $"Not a category.");
            string category = element.RetrieveProperty<string>(Codex.CATEGORY);

            // Entry without a category.
            if(category == null)
            {
                Birdsong.Sing(Birdsong.Incr(), $"This entry doesn't have a category. Added to entries without category.");
                EntriesWithoutCategory.Add(token);
                continue;
            }

            // Entry has a category. Find the token in the Categories structure
            // Then add the entry in its list.
            var categoryToken = Categories.Keys.FirstOrDefault(t => t.PayloadEntityId == category);
            Birdsong.Sing(Birdsong.Incr(), $"Its category is {category}...");
            if (categoryToken == null)
            {
                Birdsong.Sing(Birdsong.Incr(), $"It's not found. That's NOT normal. Could be a backward compatibility thing (old pre-categories save). Adding the category.");
                categoryToken = Codex.UnlockEntries(new() { category })[0];
                categoryToken.Payload.SetMutation("codex.category.open", 1, false);
            }
            Birdsong.Sing(Birdsong.Incr(), $"Adding to the list of the category {category}.");

            
            Categories.TryGetValue(categoryToken, out List<Token> categoryTokens);
            categoryTokens.Add(token);

            if(token.GetCurrentMutations().ContainsKey("codex.unread"))
            {
                UnreadCategories[categoryToken] = UnreadCategories[categoryToken] + 1;
                HasUnreadEntries = true;
            }
        }
        // We now have a dictionary of every category, the token entries in each of them, and wether each category is "unread" (new stuff to read in the category
        // entry, or one of its subentries)
        // We also know if any of them is unread.

        Birdsong.Sing(Birdsong.Incr(), $"Known Codex categories:");
        foreach (Token categoryToken in categoriesTokens)
        {
            Birdsong.Sing(Birdsong.Incr(), $"- {categoryToken.PayloadEntityId} ({Categories[categoryToken].Count} unlocked entries, unread: {UnreadCategories[categoryToken]})");
        }
    }
}
