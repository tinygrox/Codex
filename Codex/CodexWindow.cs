using System.Collections.Generic;
using System.Linq;
using Roost;
using Roost.Piebald;
using SecretHistories.Infrastructure;
using SecretHistories.Services;
using SecretHistories.UI;
using TMPro;
using UnityEngine;

class CodexWindow : AbstractMetaWindow
{
    private readonly Dictionary<Token, EntryWidgets> listEntryWidgets = new();

    private WidgetMountPoint listMountPoint;
    private TextWidget entryWidget;

    private Token selectedToken = null;
    private EntryWidgets selectedEntryWidgets = null;


    private Color categoryColor = new(0.2f, 0.8f, 0.8f, 0.1f);
    private Color selectedCategoryColor = new(0.2f, 0.8f, 0.8f, 0.3f);
    private Color entryColor = new(0, 0, 0, 0);
    private Color selectedEntryColor = new(1, 1, 1, 0.3f);

    protected override int DefaultWidth => 1200;
    protected override int DefaultHeight => 800;

    public void UpdateUI()
    {
        this.BuildEntryList();
    }

    protected override void OnAwake()
    {
        base.OnAwake();
        // TODO: Translations
        this.Title = "Codex";
        this.BuildWindow();
    }

    protected override void OnClose()
    {
        base.OnClose();
        Watchman.Get<LocalNexus>().UnForcePauseGame(false);
    }

    private void BuildWindow()
    {
        this.Icon.Clear();
        this.Icon.AddImage("Icon").SetSprite("aspect:memory");

        this.Content.Clear();
        this.Content.AddHorizontalLayoutGroup("Root")
            .SetExpandWidth()
            .SetExpandHeight()
            .SetPadding(20)
            .SetSpacing(5)
            .SetSpreadChildrenVertically()
            .AddContent(mountPoint =>
            {
                mountPoint.AddLayoutItem("ListRegion")
                    .SetMinWidth(400)
                    .SetPreferredWidth(400)
                    .SetExpandHeight()
                    .AddContent(mountPoint =>
                    {
                        mountPoint.AddImage("Background")
                            .SetIgnoreLayout()
                            .SetTop(1, 0)
                            .SetBottom(0, 0)
                            .SetLeft(0, 0)
                            .SetRight(1, 0)
                            .SetColor(new Color(0, 0, 0, 0.3f))
                            .SliceImage()
                            .SetSprite("internal:window_bg");


                        mountPoint.AddScrollRegion("ListScroll")
                            .SetVertical()
                            .SetExpandWidth()
                            .SetExpandHeight()
                            .AddContent(mountPoint =>
                            {
                                mountPoint.AddVerticalLayoutGroup("ListPadding")
                                    .SetExpandWidth()
                                    .SetExpandHeight()
                                    .SetPadding(5)
                                    .AddContent(mountPoint =>
                                    {
                                        this.listMountPoint = mountPoint.AddVerticalLayoutGroup("List")
                                            .SetSpacing(2)
                                            .SetExpandWidth()
                                            .SetExpandHeight();
                                    });

                            });
                    });

                mountPoint.AddLayoutItem("EntryRegion")
                    .SetExpandWidth()
                    .SetExpandHeight()
                    .AddContent(mountPoint =>
                    {
                        mountPoint.AddImage("Background")
                            .SetIgnoreLayout()
                            .SetTop(1, 0)
                            .SetBottom(0, 0)
                            .SetLeft(0, 0)
                            .SetRight(1, 0)
                            .SetColor(new Color(0.03f, 0.07f, 0.1f, 0.8f))
                            .SliceImage()
                            .SetSprite("internal:window_bg");

                        mountPoint.AddScrollRegion("EntryScroll")
                            .SetIgnoreLayout()
                            .SetTop(1, 0)
                            .SetBottom(0, 0)
                            .SetLeft(0, 0)
                            .SetRight(1, 0)
                            .AddContent(mountPoint =>
                            {
                                mountPoint.AddVerticalLayoutGroup("EntryPadding")
                                    .SetExpandWidth()
                                    .SetFitContentHeight()
                                    .SetPadding(10)
                                    .AddContent(mountPoint =>
                                    {
                                        this.entryWidget = mountPoint.AddText("Entry")
                                            .SetExpandWidth()
                                            .SetExpandHeight()
                                            .SetFontSize(20)
                                            .SetText("<i>Select an entry on the left to read it.</i>");
                                    });
                            });
                    });

            });

        this.BuildEntryList();
    }

    private void BuildEntryList()
    {
        this.listEntryWidgets.Clear();
        this.listMountPoint.Clear();

        var sphere = Codex.CodexSphere;
        if (sphere == null)
        {
            return;
        }

        // Insert categories
        var sortedCategories = sphere.Categories.Keys.ToList();
        sortedCategories.Sort(new TokenComparerByLabel());

        foreach (var token in sortedCategories)
        {
            this.BuildCategoryItem(token, listMountPoint);
        }

        // Insert entries without category
        var sortedUncategorisedEntries = sphere.EntriesWithoutCategory;
        sortedUncategorisedEntries.Sort(new TokenComparerByLabel());

        foreach (var token in sortedUncategorisedEntries)
        {
            this.BuildListItem(token, listMountPoint);
        }
    }

    private void BuildCategoryItem(Token token, WidgetMountPoint mountPoint)
    {
        var payload = token.Payload;

        ImageWidget backgroundWidget = null;
        TextWidget labelWidget = null;

        var row = mountPoint.AddVerticalLayoutGroup($"Entry_category_${token.Payload}")
            .SetPadding(0, 5, 0, 5)
            .SetExpandWidth()
            .AddContent(mountPoint =>
            {
                var tr = new TextRefiner(payload.GetAspects(true));
                var label = tr.RefineString(payload.Label);
                var howManyUnread = payload.Mutations.GetValueOrDefault("codex.unread", 0);
                var unread = howManyUnread > 0;

                CollapsibleDrawerWidget drawer = mountPoint.AddCollapsibleDrawer();
                backgroundWidget = drawer.HeaderBackground;
                labelWidget = drawer.HeaderText;

                drawer.Opened += (o, e) => Drawer_Opened(token);
                drawer.Closed += (o, e) => Drawer_Closed(token);

                EntryWidgets widgets = new()
                {
                    Label = labelWidget,
                    Background = backgroundWidget,
                    IsCategory = true
                };

                this.listEntryWidgets.Add(token, widgets);
                if (token == selectedToken) selectedEntryWidgets = widgets;

                drawer.Header.OnPointerClick((e) => this.SelectEntry(token));
                drawer.HeaderText.SetText((unread ? $"({howManyUnread}) " : string.Empty) + payload.Label);
                
                // Subentries
                drawer.AddContentToList(mountPoint =>
                {
                    var entries = Codex.CodexSphere.Categories[token];
                    entries.Sort(new TokenComparerByLabel());
                    foreach (Token entry in entries)
                    {
                        BuildListItem(entry, mountPoint);
                    }
                });

                if(!token.Payload.Mutations.ContainsKey("codex.category.open"))
                {
                    drawer.Close();
                }
            });
    }

    private void Drawer_Opened(Token t)
    {
        if (!t.Payload.Mutations.ContainsKey("codex.category.open")) t.Payload.SetMutation("codex.category.open", 1, false);
    }
    private void Drawer_Closed(Token t)
    {
        if(t.Payload.Mutations.ContainsKey("codex.category.open")) t.Payload.SetMutation("codex.category.open", 0, false);
    }

    private void BuildListItem(Token token, WidgetMountPoint mountPoint)
    {
        var payload = token.Payload;
        ImageWidget backgroundWidget = null;
        TextWidget labelWidget = null;
        var row = mountPoint.AddHorizontalLayoutGroup($"Entry_{token.PayloadId}")
            .SetExpandWidth()
            .SetMinHeight(40)
            .SetPreferredHeight(40)
            .WithPointerSounds()
            .SetPadding(5)
            .OnPointerClick((e) => this.SelectEntry(token))
            .AddContent(mountPoint =>
            {
                var tr = new TextRefiner(payload.GetAspects(true));
                var label = tr.RefineString(payload.Label);
                var unread = payload.Mutations.GetValueOrDefault("codex.unread", 0) > 0;

                backgroundWidget = mountPoint.AddImage("Background")
                    .SetIgnoreLayout()
                    .SetLeft(0, 0)
                    .SetRight(1, 0)
                    .SetTop(1, 0)
                    .SetBottom(0, 0)
                    .SetColor(selectedToken == token ? new Color(1, 1, 1, 0.3f) : new Color(0, 0, 0, 0))
                    .SliceImage()
                    .SetSprite("internal:window_bg");

                labelWidget = mountPoint.AddText("Label")
                    .SetExpandWidth()
                    .SetExpandHeight()
                    .SetFontSize(20)
                    .SetOverflowMode(TextOverflowModes.Ellipsis)
                    .SetVerticalAlignment(VerticalAlignmentOptions.Middle)
                    .SetFontStyle(unread ? FontStyles.Bold : FontStyles.Normal)
                    .SetText((unread ? "*" : string.Empty) + payload.Label);
            });

        EntryWidgets widgets = new()
        {
            Label = labelWidget,
            Background = backgroundWidget
        };

        this.listEntryWidgets.Add(token, widgets);
        if (token == selectedToken) selectedEntryWidgets = widgets;
    }

    private void SelectEntry(Token entry)
    {
        Codex.MarkEntryAsRead(entry);

        var tr = new TextRefiner(entry.Payload.GetAspects(true));
        string text = $"<b>{tr.RefineString(entry.Payload.Label)}</b>\n\n{tr.RefineString(entry.Payload.Description)}";
        entryWidget.SetText(text);

        if (selectedEntryWidgets != null)
        {
            Birdsong.Sing("Unselect previous");
            if (selectedEntryWidgets.IsCategory) selectedEntryWidgets.Background.SetColor(categoryColor);
            else selectedEntryWidgets.Background.SetColor(entryColor);
        }

        if (this.listEntryWidgets.TryGetValue(entry, out var widgets))
        {
            selectedToken = entry;
            selectedEntryWidgets = widgets;
            Birdsong.Sing($"Background comp= {widgets.Background}");
            if (widgets.IsCategory) widgets.Background.SetColor(selectedCategoryColor);
            else widgets.Background.SetColor(selectedEntryColor);
        }
    }

    private class EntryWidgets
    {
        public TextWidget Label { get; set; }
        public ImageWidget Background { get; set; }
        public bool IsCategory { get; set; } = false;
    }
}
